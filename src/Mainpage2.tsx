import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import "antd/dist/antd.css";
import "./index.css";
import { Layout } from "antd";
import Siderbar from "./components2/Sider";
import Headerbar from "./components2/Header";
import Contentbar from "./components2/Content";
// @ts-ignore
import { getChannels } from "mattermost-redux/actions/channels";
//@ts-ignore
import { getPosts } from "mattermost-redux/actions/posts";

interface Iprops {
  users: {};
  handleLogout: () => void;
  getChannels: (teamId: string) => void;
  getPosts: (chId: string) => any;
}
class SiderDemo extends React.Component<Iprops, {}> {
  state = {
    collapsed: false,
    messages: [],
    myData: []
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed
    });
  };

  componentDidMount() {
    this.channel();
    this.handlePosts("9qxek8kthjga5jekpx7pc6j3tw");//to make the default appear of specific channel when user logged in
  }

  channel = async () => {
    try {
      await this.props.getChannels("c3e8f3wdubnbpc9aapfujfzzdc"); //passing teamId as argument to getChannels so that we can get all channels in state.entities
    } catch (e) {
    }
  };

  handlePosts = async (chId: string) => {
    const getPostData = await this.props.getPosts(chId);//getPosts from redux store
    let messages: any[] = [];

    getPostData.data.order.map((e: any) => //mannually creating object and storing required data from redux store
      messages.push({
        channelId: getPostData.data.posts[e].channel_id,
        message: getPostData.data.posts[e].message,
        userName: getPostData.data.posts[e].props.username,
        userId: getPostData.data.posts[e].user_id,
        time: new Date(getPostData.data.posts[e].create_at).toLocaleString()
      })
    );
    let newArray = [...messages]; //storing above objects into an array
    this.setState({
      myData: newArray
    });
 
  };

  render() {
    return (
      <Layout>
        <Siderbar
          collapsed={this.state.collapsed} 
          allChannelIds={Object.keys(this.props.users)}  //channelkeys from redux store
          allChannelData={this.props.users}   //all channel data from redux store
          getPostData={this.handlePosts} //calling a function to get a particular clicked channelkey
        />
        <Layout>
          <Headerbar
            collapsed={this.state.collapsed} //collapsed icon on header
            toggle={this.toggle}
            handleLogout={this.props.handleLogout} //taking action from header and processing in login.tsx file
          />

          <Contentbar Data={this.state.myData} 
            getPostData={this.handlePosts} //sending manually created data to content page
           /> 
        </Layout>
      </Layout>
    );
  }
}
const mapStateToProps = (state: any) => {
  return {
    users: state.entities.channels.channels   //storing channels data from redux store
  };
};

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      getChannels,     //actions from redux store
      getPosts
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SiderDemo);
