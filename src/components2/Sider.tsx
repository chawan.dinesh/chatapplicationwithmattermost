import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import "antd/dist/antd.css";
import "../index.css";
//@ts-ignore
import { getPosts } from "mattermost-redux/actions/posts";
import { Layout, Menu, Icon } from "antd";
const { Sider } = Layout;

interface Iprops {
  collapsed: boolean;
  allChannelData: { [s: string]: any };
  allChannelIds: string[];
  handlePosts?: (item: string) => void;
  getPosts: (id: string) => any;
  getPostData: (id: string) => any;
}

class Siderbar extends Component<Iprops, {}> {
  state = {
    myData: []
  };

  getPost = (chId: string) => {
    this.props.getPostData(chId);  //sending channel id to the mainPage
  };

  render() {
    return (
      <Sider trigger={null} collapsible collapsed={this.props.collapsed}>
        <div className="logo" />
        <Menu theme="dark" mode="inline" defaultSelectedKeys={["0"]}>
          <Menu.Item style={{ backgroundColor: "black", marginTop: 15 }}>
            <Icon type="code" style={{ fontSize: 20, color: "skyblue" }} />
            <span
              style={{
                color: "skyblue",
                fontFamily: "cursive",
                fontWeight: "bold",
                fontSize: 25
              }}
            >
              Chat_App
            </span>
          </Menu.Item>

          {this.props.allChannelIds.map((key, ind) => ( 
            <Menu.Item
              key={ind}
              onClick={() => this.getPost(key)}
              style={{ marginTop: 8 }}
            >
              <Icon type="user" style={{ fontSize: 20, color: "white" }} />
              <span style={{ color: "white", fontWeight: "bold" }}>
                {this.props.allChannelData[key].display_name}
              </span>
            </Menu.Item>
          ))}
        </Menu>
      </Sider>
    );
  }
}


const mapStateToProps = (state: any) => {
  return {
    users: state.entities
  };
};

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      getPosts
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Siderbar);
