import React, { Component } from "react";
import "antd/dist/antd.css";
import "../index.css";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Input } from "antd";
import { Button } from "antd";
import { Layout } from "antd";
// @ts-ignore
import { createPost } from "mattermost-redux/actions/posts";
const { Content } = Layout;

interface Iprops {
  createPost: ({ key }: any) => void;
  usersId: string;
  Data: any;
  users?: any;
  getPostData: (id: string) => any;
}
interface chid {
  message: string;
  channel_id: string;
  user_id: string;
  // @ts-ignore
  props: { username: string };
}
class Contentbar extends Component<Iprops, {}> {
  state = {
    data: this.props.Data,
    text: ""
  };

  handleChange = (e: any) => {
    this.setState({
      text: e.target.value     //action from text input field
    });
  };

  handleCreatePost = async () => {
    this.setState({
      text: ""                 //after clicking submit it should clear the input text field
    });
    await this.props.createPost({    //createPost action from redux store
      message: this.state.text,      //sending input text data
      channel_id: this.props.Data[0].channelId,  //current channelid 
      user_id: this.props.usersId,   //current user id
      props: {
        username: this.props.users.profiles[this.props.usersId].username //current username
      }
    });
    this.props.getPostData(this.props.Data[0].channelId);  //it will rerenders the particular channel to appear messages
  };

  render() {
    return (
      <Content
        style={{
          margin: "24px 16px",
          padding: 24,
          background: "#fff",
          minHeight: 280,
          height: "84vh",
          overflow: "auto",
          paddingBottom: 100,
          display: "flex",
          flexDirection: "column-reverse"
        }}
      >
        {this.props.Data            //if data is present then only go to display message,username
          ? this.props.Data.map((data: any) => {
              return (
                <div>
                  {this.props.users.profiles[data.userId] ? // checking userId is present or not
                  (
                    <h2> {this.props.users.profiles[data.userId].username}</h2>
                  ) : 

                  (
                    "null"
                  )}
                  <div>
                    <div>
                      <span>
                        <p
                          style={{
                            fontSize: 15,
                            fontStyle: "italic",
                            color: "gray",
                            fontWeight: "bold"
                          }}
                        >
                          {data.message}     //display messages
                        </p>
                        <div style={{ textAlign: "right" }}>
                          <span style={{ color: "blue", fontWeight: "bold" }}>
                            {data.time}    //display time
                          </span>
                        </div>
                        <hr />
                      </span>
                    </div>
                  </div>

                  <div style={{ position: "fixed", bottom: 50 }}>
                    <Input
                      placeholder="Enter text..."
                      style={{ width: 1000 }}
                      onChange={e => this.handleChange(e)}
                      value={this.state.text}
                    />
                    <Button
                      type="primary"
                      onClick={() => this.handleCreatePost()}
                    >
                      Send
                    </Button>
                  </div>
                </div>
              );
            })
          : null}
      </Content>
    );
  }
}


const mapStateToProps = (state: any) => {
  return {
   
    usersId: state.entities.users.currentUserId,
    users: state.entities.users
  };
};

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      createPost
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Contentbar);
