import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Button } from "antd";
// @ts-ignore
import { logout } from "mattermost-redux/actions/users";
import "antd/dist/antd.css";
import "../index.css";
import { Layout, Icon } from "antd";
const { Header } = Layout;

interface Iprops {
  users: any;
  collapsed: boolean;
  toggle: () => void;
  logout: () => any;
  handleLogout: () => void;
  currentName: any;
  currentId: any;
}
export class Headerbar extends Component<Iprops, {}> {
  logout = async () => {
    const logoutData = await this.props.logout();
    logoutData.data ? this.props.handleLogout() : alert("error in logout...");
  };

  render() {
    return (
      <Header style={{ background: "black", padding: 0 }}>
        <Icon
          style={{ color: "white", fontSize: 20 }}
          className="trigger"
          type={this.props.collapsed ? "menu-unfold" : "menu-fold"}
          onClick={this.props.toggle}
        />
        <span
          style={{
            fontSize: 20,
            fontWeight: "bold",
            color: "skyblue",
            paddingLeft: 20
          }}
        >
          314e@ WELCOME__{" "}
          {this.props.currentName[this.props.currentId].username}
          <Button
            type="primary"
            onClick={this.logout}
            style={{ marginLeft: 670 }}
          >
            Logout
          </Button>
        </span>
      </Header>
    );
  }
}
const mapStateToProps = (state: any) => {
  return {
    users: state.entities,
    currentId: state.entities.users.currentUserId,
    currentName: state.entities.users.profiles
  };
};

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      logout
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Headerbar);
